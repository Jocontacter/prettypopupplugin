﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PrettyPopupDemo
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void clicked(object sender, EventArgs args)
        {
            Console.WriteLine("Showing popup!");
            //Navigation.PushModalAsync(new MyPopup());
            Rg.Plugins.Popup.Services.PopupNavigation.Instance.PushAsync(new MyPopup());
        }
    }
}
