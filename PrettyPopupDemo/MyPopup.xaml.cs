﻿using System;
using System.Collections.Generic;
using Ascetic.Plugins.PopupDecorations;
using Xamarin.Forms;

namespace PrettyPopupDemo
{
    public partial class MyPopup : PrettyPopup
    {
        public MyPopup():base(20)
        {
            InitializeComponent();
        }
    }
}
