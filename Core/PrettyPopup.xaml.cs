﻿using System;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Ascetic.Plugins.PopupDecorations.Services;
using System.Diagnostics;

namespace Ascetic.Plugins.PopupDecorations
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrettyPopup : PopupPage
    {
        public PrettyPopup()
        {
            InitializeComponent();

            _ = TakeScreenshot(Device.RuntimePlatform == Device.iOS ? 13 : 16);
        }

        /// <summary>
        /// Creates Popup with blurred background
        /// </summary>
        /// <param name="blurRadius">0-25</param>
        public PrettyPopup(int blurRadius)
        {
            InitializeComponent();

            _ = TakeScreenshot(blurRadius);
        }

        private async Task TakeScreenshot(int blurRadius)
        {
            try
            {
                var stream = await CrossScreenshotService.Current.Capture(blurRadius);
                Debug.WriteLine(string.Format("BlurredBackground image size: {0} kb", stream.Length / 1024));
                BackgroundImageSource = ImageSource.FromStream(() => stream);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
