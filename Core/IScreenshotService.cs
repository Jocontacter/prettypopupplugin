﻿using System.IO;
using System.Threading.Tasks;

namespace Ascetic.Plugins.PopupDecorations.Services
{
    public interface IScreenshotService
    {
        Task<Stream> Capture(int blurRadius);
    }
}
