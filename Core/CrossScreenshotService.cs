﻿using System;
using Xamarin.Forms;

namespace Ascetic.Plugins.PopupDecorations.Services
{
    internal static class CrossScreenshotService
    {
        static Lazy<IScreenshotService> implementation = new Lazy<IScreenshotService>(() => CreateService(),
            System.Threading.LazyThreadSafetyMode.PublicationOnly);

        /// <summary>
        /// Gets if the plugin is supported on the current platform.
        /// </summary>
        public static bool IsSupported => implementation.Value != null;

        /// <summary>
        /// Current plugin implementation to use
        /// </summary>
        public static IScreenshotService Current
        {
            get
            {
                IScreenshotService ret = implementation.Value;
                if (ret == null)
                {
                    throw NotImplementedInReferenceAssembly();
                }
                return ret;
            }
        }

        static IScreenshotService CreateService()
        {
#if NETSTANDARD1_0 || NETSTANDARD2_0 || NETSTANDARD2_1
            return DependencyService.Get<IScreenshotService>();
#else
#pragma warning disable IDE0022 // Use expression body for methods
            return new ScreenshotService();
#pragma warning restore IDE0022 // Use expression body for methods
#endif
        }

        internal static Exception NotImplementedInReferenceAssembly() =>
            new NotImplementedException("This functionality is not implemented in the portable version of this assembly.  You should reference the NuGet package from your main application project in order to reference the platform-specific implementation.");
    }
}
