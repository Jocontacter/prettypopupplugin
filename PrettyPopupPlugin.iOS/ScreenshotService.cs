﻿using System.IO;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;

using System;
using Ascetic.Plugins.PopupDecorations.iOS.Services;
using Ascetic.Plugins.PopupDecorations.Services;
using Xamarin.Forms.Internals;
using PrettyPopupPlugin.iOS;

[assembly: Dependency(typeof(ScreenshotService))]
namespace Ascetic.Plugins.PopupDecorations.iOS.Services
{
    [Preserve(AllMembers = true)]
    internal class ScreenshotService: IScreenshotService
    {
        public Task<Stream> Capture(int blurRadius=25)
        {
            blurRadius = Math.Min(25, Math.Max(blurRadius, 0));
            UIImage capture;

            using (var blurEffect = UIBlurEffect.FromStyle(BlurSettings.BlurStyle))
            {
                using (var blurWindow = new UIVisualEffectView(blurEffect))
                {
                    blurWindow.Frame = UIScreen.MainScreen.Bounds;
                    blurWindow.Alpha = Math.Min(1.0f, (1.0f / 25.0f) * blurRadius);
                    //чтобы не мерцало - добавляем не на реальный экран(screen.AddSubview(blurWindow)) а на снимок:
                    //так работает на эмуляторе но не работает на реальном устройстве:
                    //var subview = UIScreen.MainScreen.SnapshotView(true);
                    capture = UIScreen.MainScreen.Capture();
                    var subview = new UIImageView(capture);
                    subview.AddSubview(blurWindow);
                    capture = subview.Capture(true);
                    blurWindow.RemoveFromSuperview();
                    subview.Dispose();

                    return Task.FromResult(capture.AsJPEG(.6f).AsStream());
                }
            }
        }
    }
}