﻿using System;
using Ascetic.Plugins.PopupDecorations.iOS.Services;
using PrettyPopupPlugin.iOS;
using UIKit;

namespace Ascetic.Plugins.PopupDecorations.iOS
{
    /// <summary>
    /// PrettyPopup bootstrapper
    /// </summary>
    public static class PrettyPopup
    {
        internal static Type AutoInit => typeof(ScreenshotService);

        /// <summary>
        /// Initialize PrettyPopup
        /// </summary>
        /// <param name="blurStyle">blur style. By default: UIBlurEffectStyle.Regular</param>
        /// <param name="doInitRgPluginPopup">Whether Init call Rg.Plugins.Popup.Popup.Init() inside or not</param>
        public static void Init(UIBlurEffectStyle blurStyle, bool doInitRgPluginPopup = true)
        {
            BlurSettings.BlurStyle = blurStyle;
            Init(doInitRgPluginPopup);
        }

        /// <summary>
        /// Initialize PrettyPopup
        /// </summary>
        /// <param name="doInitRgPluginPopup">Whether Init call Rg.Plugins.Popup.Popup.Init() inside or not</param>
        public static void Init(bool doInitRgPluginPopup = true)
        {
            LinkAssemblies();
            if(doInitRgPluginPopup)
            {
                Rg.Plugins.Popup.Popup.Init();
            }
        }

        private static void LinkAssemblies()
        {
            if (false.Equals(true))
            {
                var i = new ScreenshotService();
                
            }
        }
    }
}
