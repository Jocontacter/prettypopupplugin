﻿using System;
using UIKit;

namespace PrettyPopupPlugin.iOS
{
    internal static class BlurSettings
    {
        internal static UIBlurEffectStyle BlurStyle { get; set; } = UIBlurEffectStyle.Regular;
    }
}
