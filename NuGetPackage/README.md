﻿
## PrettyPopup
This is small and fast decoration package for blurred background effect for your Rg.Plugins.Popups
- Install PrettyPopupPlugin and setup:
```csharp
AsceticAscetic.Plugins.PopupDecorations.<Droid/iOS>.PrettyPopup.Init()
```
PrettyPopup will automatically setup Rg.Plugins.Popup inside(you can use parameters for disable such behavior)

- Now, you can create your popup inherited from PrettyPopup and it will have blur background effect with appearance/hiding animation!

![PrettyPopup gif](https://bitbucket.org/asceticsoft/downloads/downloads/prettypopupdemo.gif)

[//]: # (<img src="https://bitbucket.org/asceticsoft/downloads/downloads/prettypopupdemo.gif" width="241" height="497" />)

### Example:
```xml
<ascetic:PrettyPopup xmlns="http://xamarin.com/schemas/2014/forms"
	xmlns:ascetic="clr-namespace:Ascetic.Plugins.PopupDecorations;assembly=PrettyPopupPlugin.Core"
	xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
	x:Class="PrettyPopupDemo.MyPopup">

    <Frame VerticalOptions="Center" CornerRadius="14">
        <Label VerticalOptions="Center" HorizontalOptions="Center" Text="My content"/>
    </Frame>

</ascetic:PrettyPopup>
```
and code behind:
```csharp
public partial class MyPopup : Ascetic.Plugins.PopupDecorations.PrettyPopup
{
    private static int BlurRadius = Device.RuntimePlatform == Device.iOS ? 15: 16;

    //parameter is optional
    public MyPopup(): base(BlurRadius)
    {
        InitializeComponent();
    }
}
```

### What's new:
v1.2.5 - fixed solid background on ios devices
v1.2.3 - fixed MonoAndroid compatibility, core lib assembly name and documentation(note the assembly name in xaml file)
v1.2.1 - possibility for customization: blur radius through popup constructor parameter(all platforms) and blur style for ios in initialization.
Example:
```csharp
AsceticAscetic.Plugins.PopupDecorations.iOS.PrettyPopup.Init(UIBlurEffectStyle.ExtraLight);
```
